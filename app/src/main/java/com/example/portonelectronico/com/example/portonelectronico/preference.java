package com.example.portonelectronico.com.example.portonelectronico;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;

import com.example.portonelectronico.R;
import com.google.android.material.textfield.TextInputEditText;

public class preference extends AppCompatActivity {
    TextInputEditText fullDownInput;
    TextInputEditText fullUpInput;
    TextInputEditText semiDownInput;
    TextInputEditText semiUpInput;
    TextInputEditText ipInput;
    SharedPreferences sp;
    private int savedFullDownTime;
    private int savedFullUpTime;
    private int savedSemiDownTime;
    private int savedSemiUpTime;
    private String savedIp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preference);
        sp = getSharedPreferences("guardar", Activity.MODE_PRIVATE);
        fullDownInput = findViewById(R.id.fulldowntime);
        fullUpInput = findViewById(R.id.fullupTime);
        semiDownInput = findViewById(R.id.semidownTime);
        semiUpInput = findViewById(R.id.semiupTime);
        ipInput = findViewById(R.id.ip);
        setData();
        putData();
    }

    private void setData() {
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        savedFullDownTime = 60;
        savedFullUpTime = 60;
        savedSemiDownTime = 30;
        savedSemiUpTime = 30;
        savedIp = "192.168.1.50";
        try {
            savedFullDownTime = sp.getInt("down", 0);
            savedFullUpTime = sp.getInt("up", 0);
            savedIp = sp.getString("ip", "ip");
            savedSemiDownTime = sp.getInt("semiDown", 0);
            savedSemiUpTime = sp.getInt("semiUp", 0);

        } catch (Error e) {
        }
        fullDownInput.setText(String.valueOf(savedFullDownTime));
        fullUpInput.setText(String.valueOf(savedFullUpTime));
        ipInput.setText(savedIp);
    }
    private void putData() {
        fullUpInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPreferences.Editor editor = sp.edit();
                Integer number = 0;
                try {
                    number = Integer.parseInt(editable.toString());
                } catch (Exception e) {

                }
                editor.putInt("up", number);
                editor.commit();
                savedFullUpTime = number;
            }
        });
        fullDownInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPreferences.Editor editor = sp.edit();
                Integer number = 0;
                try {
                    number = Integer.parseInt(editable.toString());
                } catch (Exception e) {
                }
                editor.putInt("down", number);
                editor.commit();
                savedFullDownTime = number;
            }
        });
        semiUpInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPreferences.Editor editor = sp.edit();
                Integer number = 0;
                try {
                    number = Integer.parseInt(editable.toString());
                } catch (Exception e) {

                }
                editor.putInt("semiUp", number);
                editor.commit();
                savedSemiUpTime = number;
            }
        });
        semiDownInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPreferences.Editor editor = sp.edit();
                Integer number = 0;
                try {
                    number = Integer.parseInt(editable.toString());
                } catch (Exception e) {
                }
                editor.putInt("semiDown", number);
                editor.commit();
                savedSemiDownTime = number;
            }
        });
        ipInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPreferences.Editor editor = sp.edit();
                String ip = "192.168.1.50";
                try {
                    ip = editable.toString();
                } catch (Exception e) {
                }
                editor.putString("ip", ip);
                editor.commit();
                savedIp = ip;
            }
        });
        fullDownInput.setText(String.valueOf(savedFullDownTime));
        fullUpInput.setText(String.valueOf(savedFullUpTime));
        semiDownInput.setText(String.valueOf(savedSemiDownTime));
        semiUpInput.setText(String.valueOf(savedSemiUpTime));
        ipInput.setText(savedIp);
    }

}

