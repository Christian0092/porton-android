package com.example.portonelectronico;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.example.portonelectronico.com.example.portonelectronico.preference;
import com.example.portonelectronico.com.example.portonelectronico.model.Result;
import com.example.portonelectronico.com.example.portonelectronico.service.RestClient;
import com.example.portonelectronico.com.example.portonelectronico.service.RetrofitClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.preference.PreferenceManager;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Call<Result> upCall;
    TextView fullUp;
    TextView fullDown;
    TextView semiUp;
    TextView semiDown;
    TextView ipText;
    SharedPreferences sp;
    private int savedFullDownTime;
    private int savedFullUpTime;
    private int savedSemiDownTime;
    private int savedSemiUpTime;
    private String savedIp;
    private Call<Result> downCall;
    private Call<Result> stopCall;
    FloatingActionButton fullUpButton;
    FloatingActionButton fullDownButton;
    FloatingActionButton semiUpButton;
    FloatingActionButton semiDownButton;
    FloatingActionButton stopButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        reference();
       getSaveData();
        setClickButtons();
        refresh();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent_config = new Intent(MainActivity.this, preference.class);
            startActivity(intent_config);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void refresh() {
        fullUp.setText("Tiempo de subida completo "+ String.valueOf(savedFullUpTime) + " segundos");
        fullDown.setText("Tiempo de bajada completo "+ String.valueOf(savedFullDownTime)+ " segundos");
        semiUp.setText("Tiempo de semisubida  "+ String.valueOf(savedSemiUpTime)+ " segundos");
        semiDown.setText("Tiempo de semibajada "+ String.valueOf(savedSemiDownTime)+ " segundos");
        ipText.setText(savedIp);
    }

    private void showToast(Response<Result> data) {
        Integer value = data.body().getReturn_value();
        if (value == 1 & !value.equals(null)) {
            Toast.makeText(getApplicationContext(), "Realizado correctamente", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "El porton esta ocupado", Toast.LENGTH_SHORT).show();
        }
    }

    private void showError() {
        Toast.makeText(getApplicationContext(), "Error al conectarse con el porton, compruebe la ip", Toast.LENGTH_SHORT).show();
    }

    private void eneableButtons() {
        fullUpButton.setEnabled(true);
        fullDownButton.setEnabled(true);
        stopButton.setEnabled(true);
        semiDownButton.setEnabled(true);
        semiUpButton.setEnabled(true);
    }

    private void disableButtons() {
        fullUpButton.setEnabled(false);
        fullDownButton.setEnabled(false);
        stopButton.setEnabled(false);
        semiDownButton.setEnabled(false);
        semiUpButton.setEnabled(false);
    }

    private void setClickButtons() {
        fullUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
                RestClient service = RetrofitClient.getRetrofitInstance("http://" + savedIp);
                upCall = service.subir(String.valueOf(savedFullUpTime * 1000));
                disableButtons();
                try {
                    upCall.clone().enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            showToast(response);
                            eneableButtons();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            showError();
                            eneableButtons();
                        }
                    });
                } catch (Error e) {
                    eneableButtons();
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                }

            }
        });
        fullDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
                RestClient service = RetrofitClient.getRetrofitInstance("http://" + savedIp);
                downCall = service.bajar(String.valueOf(savedFullDownTime * 1000));
                disableButtons();
                try {
                    downCall.clone().enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            showToast(response);
                            eneableButtons();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            showError();
                            eneableButtons();
                        }
                    });
                } catch (Error e) {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    eneableButtons();
                }
            }
        });

        semiUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
                RestClient service = RetrofitClient.getRetrofitInstance("http://" + savedIp);
                upCall = service.subir(String.valueOf(savedSemiUpTime * 1000));
                disableButtons();
                try {
                    upCall.clone().enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            showToast(response);
                            eneableButtons();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            showError();
                            eneableButtons();
                        }
                    });
                } catch (Error e) {
                    eneableButtons();
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                }

            }
        });
        semiDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
                RestClient service = RetrofitClient.getRetrofitInstance("http://" + savedIp);
                downCall = service.bajar(String.valueOf(savedSemiDownTime * 1000));
                disableButtons();
                try {
                    downCall.clone().enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            showToast(response);
                            eneableButtons();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            showError();
                            eneableButtons();
                        }
                    });
                } catch (Error e) {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    eneableButtons();
                }
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
                RestClient service = RetrofitClient.getRetrofitInstance("http://" + savedIp);
                stopCall = service.parar();
                disableButtons();
                try {
                    stopCall.clone().enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            showToast(response);
                            eneableButtons();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            showError();
                            eneableButtons();
                        }
                    });
                } catch (Error e) {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    eneableButtons();
                }
            }
        });
    }

    private void reference() {
        fullUpButton = findViewById(R.id.fab);
        fullDownButton = findViewById(R.id.buttonDown);
        semiUpButton = findViewById(R.id.lessButtonUp);
        semiDownButton = findViewById(R.id.lessButtonDown);
        stopButton = findViewById(R.id.stopButton);

        fullUp = findViewById(R.id.fullUpTime);
        fullDown = findViewById(R.id.fullDownTIme);
        semiUp = findViewById(R.id.semiUpTime);
        semiDown = findViewById(R.id.semiDownTime);
        ipText = findViewById(R.id.Ip);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        getSaveData();
        refresh();
        super.onWindowFocusChanged(hasFocus);
    }
    private void getSaveData(){
        sp = getSharedPreferences("guardar", Activity.MODE_PRIVATE);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        savedFullDownTime = 2;
        savedFullUpTime = 2;
        savedSemiDownTime = 2;
        savedSemiUpTime = 2;
        savedIp = "192.168.1.50";
        try {
            savedFullDownTime = sp.getInt("down", 0);
            savedFullUpTime = sp.getInt("up", 0);
            savedIp = sp.getString("ip", "ip");
            savedSemiDownTime = sp.getInt("semiDown", 0);
            savedSemiUpTime = sp.getInt("semiUp", 0);

        } catch (Error e) {
        }
    }
}
