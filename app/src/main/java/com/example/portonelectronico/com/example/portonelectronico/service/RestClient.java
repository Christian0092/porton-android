package com.example.portonelectronico.com.example.portonelectronico.service;

import com.example.portonelectronico.com.example.portonelectronico.model.Result;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestClient {
    @GET("/subir")
    Call<Result> subir(@Query("params") String time);
    @GET("/bajar")
    Call<Result> bajar(@Query("params") String time);
    @GET("/parar?params=6000")
    Call<Result> parar();

}
